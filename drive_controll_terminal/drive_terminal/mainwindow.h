#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "iconnect.h"
#include "imachine_controll.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, iconnect,imachine_controll
{
    Q_OBJECT
    Q_INTERFACES(iconnect)
    Q_INTERFACES(imachine_controll)

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void net_connect (QHostAddress,quint16 port);
    void net_disconnet ();

    void machine_direction (direction_machine_controll_t);

public slots:
    void net_on_connected(void);
    void net_on_disconnected (void);

private slots:
    void on_pushButton_disconnect_clicked();

    void on_pushButton_connect_clicked();

    void on_pushButton_forward_clicked(bool checked);

    void on_pushButton_stop_clicked(bool checked);

    void on_pushButton_left_clicked(bool checked);

    void on_pushButton_right_clicked(bool checked);

    void on_pushButton_backward_clicked(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
