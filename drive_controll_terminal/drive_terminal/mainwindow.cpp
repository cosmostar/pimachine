#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iconnect.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)

{
    ui->setupUi(this);
    this->ui->pushButton_disconnect->setDisabled(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_connect_clicked()
{
    bool validation = 1;

    QHostAddress addr(this->ui->lineEdit_address->text());
    quint16 port = this->ui->lineEdit_port->text().toUInt();


    if (validation)
       emit this->net_connect(addr,port);
}




void MainWindow::net_on_connected()
{
    this->ui->pushButton_connect->setDisabled(1);
    this->ui->pushButton_disconnect->setDisabled(0);

    this->ui->label_status->setText("Connected");

}


void MainWindow::net_on_disconnected()
{
    this->ui->pushButton_connect->setDisabled(0);
    this->ui->pushButton_disconnect->setDisabled(1);
    this->ui->label_status->setText("Disconnected");

}

void MainWindow::on_pushButton_disconnect_clicked()
{
    emit this->net_disconnet();
}

void MainWindow::on_pushButton_forward_clicked(bool checked)
{
    if (checked)
        emit this->machine_direction(machine_forward);
    else
        emit this->machine_direction(machine_stop);
}

void MainWindow::on_pushButton_stop_clicked(bool checked)
{
      emit this->machine_direction(machine_stop);
}



void MainWindow::on_pushButton_left_clicked(bool checked)
{
    if (checked)
        emit this->machine_direction(machine_left);
    else
        emit this->machine_direction(machine_stop);
}

void MainWindow::on_pushButton_right_clicked(bool checked)
{
    if (checked)
        emit this->machine_direction(machine_right);
    else
        emit this->machine_direction(machine_stop);
}

void MainWindow::on_pushButton_backward_clicked(bool checked)
{
    if (checked)
        emit this->machine_direction(machine_backward);
    else
        emit this->machine_direction(machine_stop);
}
