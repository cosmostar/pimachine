#include "tcp_client.h"

#include <QHostAddress>

tcp_client::tcp_client()
{

}

void tcp_client::connect_to_host(QHostAddress addr, quint16 port)
{
    this->connectToHost(addr,port);
}

void tcp_client::disconnect()
{
    this->disconnectFromHost();
}

void tcp_client::send_command(direction_machine_controll_t dir)
{
    quint8 d = dir;
    this->write((char *)&d,1);
}


