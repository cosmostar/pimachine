#ifndef IMACHINE_CONTROLL_H
#define IMACHINE_CONTROLL_H

#include "../../drive_protocol/drive_proto.h"


class imachine_controll
{
public:

signals:
    virtual void machine_direction (direction_machine_controll_t) = 0;

};


Q_DECLARE_INTERFACE(imachine_controll,"imachine_controll")

#endif // IMACHINE_CONTROLL_H
