#-------------------------------------------------
#
# Project created by QtCreator 2015-11-16T15:42:01
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = drive_terminal
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    tcp_client.cpp

HEADERS  += mainwindow.h \
    iconnect.h \
    tcp_client.h \
    imachine_controll.h

FORMS    += mainwindow.ui
