#include "mainwindow.h"

#include <QApplication>

#include "tcp_client.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    tcp_client client;


    QObject::connect(&client,SIGNAL(connected()),&w,SLOT(net_on_connected()));
    QObject::connect(&client,SIGNAL(disconnected()),&w,SLOT(net_on_disconnected()));

    QObject::connect(&w,SIGNAL(net_connect(QHostAddress,quint16)),
                      &client,SLOT(connect_to_host(QHostAddress,quint16)));
    QObject::connect(&w,SIGNAL(net_disconnet()), &client, SLOT(disconnect()));

    QObject::connect(&w,SIGNAL(machine_direction(direction_machine_controll_t)),
                     &client, SLOT(send_command(direction_machine_controll_t)));


    return a.exec();
}
