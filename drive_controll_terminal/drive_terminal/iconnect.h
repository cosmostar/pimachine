#ifndef ICONNECT_H
#define ICONNECT_H

#include <QHostAddress>

class iconnect
{

signals:

    virtual void net_connect (QHostAddress,quint16 port) =0;
    virtual void net_disconnet () = 0;


public slots:

    virtual void net_on_connected(void) = 0;
    virtual void net_on_disconnected (void) = 0;

};

Q_DECLARE_INTERFACE(iconnect,"iconnect")

#endif // ICONNECT_H
