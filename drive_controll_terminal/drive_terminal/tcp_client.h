#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <QHostAddress>
#include <QTcpSocket>

#include "imachine_controll.h"

class tcp_client : public QTcpSocket
{
    Q_OBJECT

public:
    tcp_client();

public slots:
    void connect_to_host (QHostAddress addr, quint16 port);
    void disconnect ();

    void send_command (direction_machine_controll_t dir);
signals:

};

#endif // TCP_CLIENT_H
