/* 
 * File:   pwm_motor.h
 * Author: lemz
 *
 * Created on September 25, 2015, 11:52 AM
 */

#ifndef PWM_MOTOR_H
#define	PWM_MOTOR_H

#include <sys/time.h>

#include "presets.h"
struct pwm_motor 
{
    
    int pwm_promille;
    
    unsigned quant_usecs_size =100;

    bool get_value (struct timeval current_time);
    
     void disable (void);
    
private:
    
    struct timeval last_updated;
    bool on = 0;
    int ppm =0 ;
    bool last_value = 0;
    
};

struct motor
{
    pwm_motor pwm;
    motor_pinout * pinout = NULL;
    bool reverse = 0;
    
};
#endif	/* PWM_MOTOR_H */

