#ifndef DRIVE_PROTO_H_
#define DRIVE_PROTO_H_
enum direction_machine_controll_t {
    machine_stop = 0,
    machine_backward = 1,
    machine_forward = 2,
    machine_right = 3,
    machine_left = 4

};

#endif
