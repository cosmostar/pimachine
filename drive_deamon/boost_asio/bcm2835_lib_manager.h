/* 
 * File:   bcm2835_lib_manager.h
 * Author: lemz
 *
 * Created on September 24, 2015, 12:14 PM
 */

#ifndef BCM2835_LIB_MANAGER_H
#define	BCM2835_LIB_MANAGER_H

#include <bcm2835.h>

class bcm2835_lib_manager {

public:
    bcm2835_lib_manager()
    {
        bcm2835_init();
    }
    ~bcm2835_lib_manager()
    {
        bcm2835_close();
    }
    
    
};



#endif	/* BCM2835_LIB_MANAGER_H */

