/* 
 * File:   presets.h
 * Author: lemz
 *
 * Created on September 22, 2015, 3:09 PM
 */

#ifndef PRESETS_H
#define	PRESETS_H

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <boost/program_options.hpp>
#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include "config.h"


enum motor_side_t
{
    front_right = 0,
    front_left = 1,
    rear_right = 2,
    rear_left = 3
};

#define MOTOR_SIDE_COUNT 4

struct motor_pinout
{
    unsigned int pin1;
    unsigned int pin2;
    unsigned int pine;
};

struct motors_pinout
{
    
      motor_pinout motors[4] {{
    FRONT_RIGHT_DRIVE_PIN1_DEFAULT,
  FRONT_RIGHT_DRIVE_PIN2_DEFAULT,
   FRONT_RIGHT_DRIVE_PINE_DEFAULT},
    
    {
   FRONT_LEFT_DRIVE_PIN1_DEFAULT,
    FRONT_LEFT_DRIVE_PIN2_DEFAULT,
  FRONT_LEFT_DRIVE_PINE_DEFAULT},
    
    {
   REAR_RIGHT_DRIVE_PIN1_DEFAULT,
    REAR_RIGHT_DRIVE_PIN2_DEFAULT,
  REAR_RIGHT_DRIVE_PINE_DEFAULT},
    
    {
 REAR_LEFT_DRIVE_PIN1_DEFAULT,
  REAR_LEFT_DRIVE_PIN2_DEFAULT,
 REAR_LEFT_DRIVE_PINE_DEFAULT}};

};


class presets : public motors_pinout {
public:
    presets();
    presets(std::string);
    bool load (std::string );
    presets(const presets& orig);
    virtual ~presets();  
    
    boost::property_tree::ptree pt;

   
private:
    
    

#define LOAD_VALUE_MOTOR(motor,val) __load_value ( motors[motor].val ,std::string( #motor "." #val ) , motors[motor].val )  
    
#define LOAD_VALUE(val) __load_value (val,std::string(#val),val)  

#define LOAD_VALUE_DEF(val,def) __load_value (val,std::string(#val),def)  
    
    
template< typename T >void  __load_value ( T& , std::string name,T  default_value);
    
};

#endif	/* PRESETS_H */

