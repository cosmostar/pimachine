/* 
 * File:   pwm_module.h
 * Author: lemz
 *
 * Created on September 23, 2015, 3:13 PM
 */

#ifndef PWM_MODULE_H
#define	PWM_MODULE_H

#include <vector>
#include <map>
#include <boost/thread.hpp>
#include <stdio.h>

#include "presets.h"
#include "pwm_motor.h"

#include <sys/time.h>
#include <bcm2835.h>








class pwm_module {
public:
    pwm_module(motors_pinout);
    pwm_module(const pwm_module& orig);
    virtual ~pwm_module();
    
    void loop (void);
    void start (void);
    
    void init (void);
    void deinit (void);
    
    void poll(void);
    
    void set_pwm (motor_side_t,int promile);
    void set_pwm (int * promile,unsigned int count );
    
    
    motor motors [4];
    
private:
    motors_pinout pins;
    
    boost::thread* thr;

    void gpio_init(void);
    void gpio_deinit (void);
    
};

#endif	/* PWM_MODULE_H */

