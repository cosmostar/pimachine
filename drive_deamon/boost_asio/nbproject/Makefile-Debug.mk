#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc-4.8
CCC=g++-4.8
CXX=g++-4.8
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/bcm2835_lib_manager.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/presets.o \
	${OBJECTDIR}/pwm_module.o \
	${OBJECTDIR}/pwm_motor.o


# C Compiler Flags
CFLAGS=-std=gnu++11

# CC Compiler Flags
CCFLAGS=-std=gnu++11
CXXFLAGS=-std=gnu++11

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Lgpio -lboost_system -lboost_program_options -lboost_thread -lbcm2835 -lpthread

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/boost_asio

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/boost_asio: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/boost_asio ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/bcm2835_lib_manager.o: bcm2835_lib_manager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igpio -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bcm2835_lib_manager.o bcm2835_lib_manager.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igpio -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/presets.o: presets.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igpio -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/presets.o presets.cpp

${OBJECTDIR}/pwm_module.o: pwm_module.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igpio -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/pwm_module.o pwm_module.cpp

${OBJECTDIR}/pwm_motor.o: pwm_motor.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Igpio -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/pwm_motor.o pwm_motor.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/boost_asio

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
