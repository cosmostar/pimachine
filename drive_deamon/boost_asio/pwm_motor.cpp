/* 
 * File:   pwm_motor.cpp
 * Author: lemz
 * 
 * Created on September 25, 2015, 11:52 AM
 */

#include "pwm_motor.h"

#include <math.h>



bool pwm_motor::get_value(struct timeval current_time) {
    if (!on) {
        last_updated = current_time;
        last_value = 0;
        on = 1;
        return last_value;
    }

    unsigned int usecs = (current_time.tv_sec - last_updated.tv_sec) * 1000000 + (current_time.tv_usec - last_updated.tv_usec);

    if (last_value)
        ppm = +usecs * abs(1000 - pwm_promille);
    else
        ppm -= usecs * abs(pwm_promille);
   
    last_value = (ppm < 0);
    last_updated = current_time;
   // printf ("%d\n",last_value);
    
    return last_value;
}

void pwm_motor::disable(void) {
    this->on = 0;
}
