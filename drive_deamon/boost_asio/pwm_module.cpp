/* 
 * File:   pwm_module.cpp
 * Author: lemz
 * 
 * Created on September 23, 2015, 3:13 PM
 */

#include "pwm_module.h"

pwm_module::pwm_module(motors_pinout p) : pins(p) {
    
    motors[front_left].pinout = &(p.motors[front_left]);
    motors[front_right].pinout = &(p.motors[front_right]);
    motors[rear_left].pinout = &(p.motors[rear_left]);
    motors[rear_right].pinout = &(p.motors[rear_right]);
    
    gpio_init();
    
    for (int i =0; i < MOTOR_SIDE_COUNT ; i++)
    {
             
        bcm2835_gpio_write(this->pins.motors[i].pine, 0);
        bcm2835_gpio_write(this->pins.motors[i].pin1, 0);
        bcm2835_gpio_write(this->pins.motors[i].pin2, 0);
      
    }
    
    
        
}



pwm_module::pwm_module(const pwm_module& orig) {
    
    gpio_init();
}

pwm_module::~pwm_module() {
    gpio_deinit();
}

void pwm_module::poll()
{
    
    struct timeval tv;
    gettimeofday(&tv,NULL);
    
    for (int i =0; i < MOTOR_SIDE_COUNT ; i++)
    {
        bool on_off = motors[i].pwm.get_value(tv);
    
        
        bool p1,p2;
    
        p1 = on_off && motors[i].reverse;
        p2 = on_off && !motors[i].reverse;
             
        bcm2835_gpio_write(this->pins.motors[i].pine, 1);
        bcm2835_gpio_write(this->pins.motors[i].pin1, p1);
        bcm2835_gpio_write(this->pins.motors[i].pin2, p2);
      
    }
    
    usleep(1);

}


void pwm_module::loop()
{
    while (1)
    {
        this->poll();
    }
}

void pwm_module::start (void)
{
    thr = new boost::thread(boost::bind(&pwm_module::loop, this));
}

void pwm_module :: gpio_init (void)
{
    for (int i = 0; i <MOTOR_SIDE_COUNT; i++ )
    {
      bcm2835_gpio_fsel(this->pins.motors[i].pin1, BCM2835_GPIO_FSEL_OUTP);   
      bcm2835_gpio_fsel(this->pins.motors[i].pin2, BCM2835_GPIO_FSEL_OUTP);   
      bcm2835_gpio_fsel(this->pins.motors[i].pine, BCM2835_GPIO_FSEL_OUTP);   
    }
}

void pwm_module :: gpio_deinit (void)
{
    for (int i = 0; i <MOTOR_SIDE_COUNT; i++ )
    {
      bcm2835_gpio_fsel(this->pins.motors[i].pin1, BCM2835_GPIO_FSEL_INPT);   
      bcm2835_gpio_fsel(this->pins.motors[i].pin2, BCM2835_GPIO_FSEL_INPT);   
      bcm2835_gpio_fsel(this->pins.motors[i].pine, BCM2835_GPIO_FSEL_INPT);   
    }

}


void pwm_module::set_pwm(motor_side_t sd, int promile)
{
    this->motors[sd].pwm.pwm_promille = abs(promile);
    this->motors[sd].reverse  = (promile > 0);
    
}

void pwm_module::set_pwm( int* promile,unsigned int count)
{
    for ( unsigned int i  =0 ; (i < count) && (i < MOTOR_SIDE_COUNT); i ++)
        pwm_module::set_pwm((motor_side_t)i, promile[i]);
}