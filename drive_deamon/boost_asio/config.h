/* 
 * File:   config.h
 * Author: lemz
 *
 * Created on September 22, 2015, 12:35 PM
 */

#ifndef CONFIG_H
#define	CONFIG_H

#ifdef	__cplusplus
extern "C" {
#endif

#define DEFAULT_CONFIG_PATH "/etc/pirobocar/config.ini"
#define DEFAULT_HOST_ADDRESS "127.0.0.1"
#define DEFAULT_PORT 9001
    
#define MOTOR_SECTION_NAME "motors_pinout"
    
    
#define    PIN1_NAME pin1
#define    PIN2_NAME pin2
#define    PINE_NAME pine
    
    
    

#define    FRONT_RIGHT_DRIVE_PIN1_DEFAULT  2
#define    FRONT_RIGHT_DRIVE_PIN2_DEFAULT  2
#define    FRONT_RIGHT_DRIVE_PINE_DEFAULT  3


#define    FRONT_LEFT_DRIVE_PIN1_DEFAULT  4
#define    FRONT_LEFT_DRIVE_PIN2_DEFAULT  5
#define    FRONT_LEFT_DRIVE_PINE_DEFAULT  6


#define    REAR_RIGHT_DRIVE_PIN1_DEFAULT  7
#define    REAR_RIGHT_DRIVE_PIN2_DEFAULT  8
#define    REAR_RIGHT_DRIVE_PINE_DEFAULT  9


#define    REAR_LEFT_DRIVE_PIN1_DEFAULT  10
#define    REAR_LEFT_DRIVE_PIN2_DEFAULT  11
#define    REAR_LEFT_DRIVE_PINE_DEFAULT  12


#ifdef	__cplusplus
}
#endif
#endif	/* CONFIG_H */

