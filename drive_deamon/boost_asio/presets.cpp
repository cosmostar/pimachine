/* 
 * File:   presets.cpp
 * Author: lemz
 * 
 * Created on September 22, 2015, 3:09 PM
 */

#include "presets.h"

#include <iostream>

#include <boost/program_options/detail/config_file.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "config.h"

presets::presets()  {
}

presets::presets(const presets& orig) {
}

presets::~presets() {
}

bool presets::load(std::string  filename)
{
    bool rc = 0;
    std::ifstream file(filename.c_str());
    if (!file.is_open())
    {
        std::cout<< " no presets file detected";
        exit (1);
        return 0;
    }
    else
    {
        std::cout <<"file with presets detected "<< filename;
    }


    boost::property_tree::ini_parser::read_ini(file, pt);
    
    LOAD_VALUE_MOTOR(front_right,pin1);
    LOAD_VALUE_MOTOR(front_right,pin2);
    LOAD_VALUE_MOTOR(front_right,pine);
    
    LOAD_VALUE_MOTOR(front_left,pin1);
    LOAD_VALUE_MOTOR(front_left,pin2);
    LOAD_VALUE_MOTOR(front_left,pine);
    
    LOAD_VALUE_MOTOR(rear_right,pin1);
    LOAD_VALUE_MOTOR(rear_right,pin2);
    LOAD_VALUE_MOTOR(rear_right,pine);
    
    LOAD_VALUE_MOTOR(rear_left,pin1);
    LOAD_VALUE_MOTOR(rear_left,pin2);
    LOAD_VALUE_MOTOR(rear_left,pine);
    
    file.close();
    
}



template< typename T > void presets::__load_value(T & value, std::string name,T  default_value)
{
  value =  this->pt.get(boost::property_tree::path(name),default_value);
  std::cout << "value loaded :" << name << " : " << value;
}