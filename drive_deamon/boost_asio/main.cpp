/* 
 * File:   main.cpp
 * Author: lemz
 *
 * Created on September 22, 2015, 12:29 PM
 */

#include <cstdlib>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <boost/asio/ip/address_v4.hpp>
#include <boost/thread/thread.hpp>
#include <string.h>
#include <string>
#include <iostream>

#include "config.h"
#include "presets.h"
#include "pwm_module.h"
#include "drive_proto.h"

/*
 * 
 */
void client_session(boost::shared_ptr<boost::asio::ip::tcp::socket> sock,pwm_module pwm) {
    while (true) {
        char data[512];
        size_t len = sock->read_some(boost::asio::buffer(data));
        
                switch (data[0])
        {
            case     machine_stop:
                pwm.set_pwm (front_right,0);
                pwm.set_pwm (front_left,0);
                pwm.set_pwm (rear_right,0);
                pwm.set_pwm (rear_left,0);
                
        
                break;
                
            case     machine_forward:
                
                pwm.set_pwm (front_right,100);
                pwm.set_pwm (front_left,100);
                pwm.set_pwm (rear_right,100);
                pwm.set_pwm (rear_left,100);
                break;
            case machine_backward:
                
                pwm.set_pwm (front_right,-100);
                pwm.set_pwm (front_left,-100);
                pwm.set_pwm (rear_right,-100);
                pwm.set_pwm (rear_left,-100);
                break;
            case machine_left:
                
                pwm.set_pwm (front_right,-100);
                pwm.set_pwm (front_left,100);
                pwm.set_pwm (rear_right,-100);
                pwm.set_pwm (rear_left,100);
                break;
            case machine_right:
                
                pwm.set_pwm (front_right,100);
                pwm.set_pwm (front_left,-100);      
                pwm.set_pwm (rear_right,100);
                pwm.set_pwm (rear_left,-100);
                break;
            
                
           
                        
                
                 
        }
        
        
        if (len > 0)
            write(*sock, boost::asio::buffer("ok", 2));
    }
}

int main(int argc, char** argv) {

    std::string filename_presets;
    unsigned int port;
    
    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce help message")
            ("config,c", 
            boost::program_options::value<std::string>(&filename_presets)->default_value(DEFAULT_CONFIG_PATH),
            "path to config file" )
            ("test", boost::program_options::value<std::string>(), "test: run all engines")
            ("port,p", boost::program_options::value<unsigned int>(&port)->default_value(DEFAULT_PORT), "port")
            ("version", "Version of program");

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    

    boost::asio::ip::address_v4 address = boost::asio::ip::address_v4::from_string(DEFAULT_HOST_ADDRESS);
    std::string str;
    if (vm.count("help")) {
        std::cout << desc << "\n";
        exit(0);
    }

    presets sets;
    
    
     
     
    if (vm.count("port")) {
        std::cout << "port is " << port << "\n";

    } else {
        port = sets.pt.get("common.port", 2222U);
    }




    if (vm.count("host")) {
        address = boost::asio::ip::address_v4::from_string(vm["host"].as<std::string>().c_str());
        std::cout << "host address is " << address.to_string() << "\n";

        if (address.to_string() == vm["host"].as<std::string>()) {
            std::cout << "host address is " << address.to_string() << "\n";

        } else {
            std::cerr << "cannot parse ip address" << std::endl;
            address = boost::asio::ip::address_v4::from_string(DEFAULT_HOST_ADDRESS);
        }
    }





    pwm_module pwm(sets);

    if (vm.count("test")) {
        int pwms [4] = {100, 100, 100, 100};
        pwm.set_pwm((int *) &pwms, 4);

    }
    else {
    }

    pwm.start();



    boost::asio::io_service service;
    boost::asio::ip::tcp::endpoint ep(address, port);
    boost::asio::ip::tcp::socket sock(service);
    boost::asio::ip::tcp::acceptor acc(service, ep);



    while (true) {
        boost::shared_ptr<boost::asio::ip::tcp::socket> sock(new boost::asio::ip::tcp::socket(service));
        acc.accept(*sock);
        boost::thread(boost::bind(client_session, sock,pwm));
    }






    return 0;
}

